# NexSrcipt

[![Build Status](https://travis-ci.org/Bitcoin-com/nexscript.svg)](https://travis-ci.org/Bitcoin-com/nexscript)
[![Coverage Status](https://img.shields.io/codecov/c/github/Bitcoin-com/nexscript.svg)](https://codecov.io/gh/Bitcoin-com/nexscript/)
[![NPM Version](https://img.shields.io/npm/v/nexscript.svg)](https://www.npmjs.com/package/nexscript)
[![NPM Monthly Downloads](https://img.shields.io/npm/dm/nexscript.svg)](https://www.npmjs.com/package/nexscript)
[![NPM License](https://img.shields.io/npm/l/nexscript.svg)](https://www.npmjs.com/package/nexscript)

NexSrcipt is a high-level programming language for smart contracts on Bitcoin Cash. It offers a strong abstraction layer over Bitcoin Cash' native virtual machine, Bitcoin Script. Its syntax is based on Ethereum's smart contract language Solidity, but its functionality is very different since smart contracts on Bitcoin Cash differ greatly from smart contracts on Ethereum. For a detailed comparison of them, refer to the blog post [*Smart Contracts on Ethereum, Bitcoin and Bitcoin Cash*](https://kalis.me/smart-contracts-eth-btc-bch/).

This repository contains the code for the NexSrcipt compiler & command line tool under [`packages/nexc/`](/packages/nexc). This repository also contains the code for the NexSrcipt JavaScript SDK under [`packages/nexscript/`](/packages/nexscript). The source code of the [NexSrcipt.org](https://nexscript.org) website is included under [`website/`](/website). Visit the website for a detailed [Documentation](https://nexscript.org/docs/) on the NexSrcipt language and SDK.

## The NexSrcipt Language
NexSrcipt is a high-level language that allows you to write Bitcoin Cash smart contracts in a straightforward and familiar way. Its syntax is inspired by Ethereum's Solidity language, but its functionality is different since the underlying systems have very different fundamentals. See the [language documentation](https://nexscript.org/docs/language/) for a full reference of the language.

## The NexSrcipt Compiler
NexSrcipt features a compiler as a standalone command line tool, called `nexc`. It can be installed through npm and used to compile `.nex` files into `.json` artifact files. These artifact files can be imported into the NexSrcipt JavaScript SDK (or other SDKs in the future). The `nexc` NPM package can also be imported inside JavaScript files to compile `.nex` files without using the command line tool.

### Installation
```bash
npm install -g nexc
```

### Usage
```bash
Usage: nexc [options] [source_file]

Options:
  -V, --version        Output the version number.
  -o, --output <path>  Specify a file to output the generated artifact.
  -h, --hex            Compile the contract to hex format rather than a full artifact.
  -A, --asm            Compile the contract to ASM format rather than a full artifact.
  -c, --opcount        Display the number of opcodes in the compiled bytecode.
  -s, --size           Display the size in bytes of the compiled bytecode.
  -?, --help           Display help
```

## The NexSrcipt SDK
The main way to interact with NexSrcipt contracts and integrate them into applications is using the NexSrcipt SDK. This SDK allows you to import `.json` artifact files that were compiled using the `nexc` compiler and convert them to `Contract` objects. These objects are used to create new contract instances. These instances are used to interact with the contracts using the functions that were implemented in the `.nex` file. For more information on the NexSrcipt SDK, refer to the [SDK documentation](https://nexscript.org/docs/sdk/).

### Installation
```bash
npm install nexscript
```

### Usage
```ts
import { Contract, ... } from '@nexscript/nexscript';
```

Using the NexSrcipt SDK, you can import contract artifact files, create new instances of these contracts, and interact with these instances:

```ts
...
  // Import the P2PKH artifact
  import P2PKH from './p2pkh-artifact.json' assert { type: 'json' };

  // Instantiate a network provider for NexSrcipt's network operations
  const provider = new ElectrumNetworkProvider('mainnet');

  // Create a new P2PKH contract with constructor arguments: { pkh: pkh }
  const contract = new Contract(P2PKH, [pkh], provider);

  // Get contract balance & output address + balance
  console.log('contract address:', contract.address);
  console.log('contract balance:', await contract.getBalance());

  // Call the spend function with the owner's signature
  // And use it to send 0. 000 100 00 BCH back to the contract's address
  const txDetails = await contract.functions
    .spend(pk, new SignatureTemplate(keypair))
    .to(contract.address, 10000)
    .send();

  console.log(txDetails);
...
```

## Examples
If you want to see NexSrcipt in action and check out its usage, there are several example contracts in the [`examples/`](/examples) directory. The `.nex` files contain example contracts, and the `.ts` files contain example usage of the NexSrcipt SDK to interact with these contracts.

The "Hello World" of NexSrcipt contracts is defining the P2PKH pattern inside a contract, which can be found under [`examples/p2pkh.nex`](/examples/p2pkh.nex). Its usage can be found under [`examples/p2pkh.ts`](/examples/p2pkh.ts).

### Running the examples
To run the examples, clone this repository and navigate to the `examples/` directory. Since the examples depend on the SDK, be sure to run `npm install` or `yarn` inside the `examples/` directory, which installs all required packages.

```bash
git clone git@github.com:Bitcoin-com/nexscript.git
cd nexscript/examples
npm install
```

All `.ts` files in the [`examples/`](/examples) directory can then be executed with `ts-node`.

```bash
npm install -g ts-node
ts-node p2pkh.ts
```

All `.js` files can be executed with `node`.

```bash
node p2pkh.js
```
