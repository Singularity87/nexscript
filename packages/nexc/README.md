# NexScript

[![Build Status](https://travis-ci.org/Bitcoin-com/nexscript.svg)](https://travis-ci.org/Bitcoin-com/nexscript)
[![Coverage Status](https://img.shields.io/codecov/c/github/Bitcoin-com/nexscript.svg)](https://codecov.io/gh/Bitcoin-com/nexscript/)
[![NPM Version](https://img.shields.io/npm/v/nexscript.svg)](https://www.npmjs.com/package/nexscript)
[![NPM Monthly Downloads](https://img.shields.io/npm/dm/nexscript.svg)](https://www.npmjs.com/package/nexscript)
[![NPM License](https://img.shields.io/npm/l/nexscript.svg)](https://www.npmjs.com/package/nexscript)

NexScript is a high-level programming language for smart contracts on Nexa Blockchain. It offers a strong abstraction layer over Nexa' native virtual machine, Bitcoin Script. Its syntax is based on Ethereum's smart contract language Solidity, but its functionality is very different since smart contracts on Nexa differ greatly from smart contracts on Ethereum. For a detailed comparison of them, refer to the blog post [*Smart Contracts on Ethereum, Bitcoin and Nexa*](https://kalis.me/smart-contracts-eth-btc-bch/).

See the [GitHub repository](https://github.com/Bitcoin-com/nexscript) and the [NexScript website](https://nexscript.org) for full documentation and usage examples.

## The NexScript Language
NexScript is a high-level language that allows you to write Bitcoin Cash smart contracts in a straightforward and familiar way. Its syntax is inspired by Ethereum's Solidity language, but its functionality is different since the underlying systems have very different fundamentals. See the [language documentation](https://nexscript.org/docs/language/) for a full reference of the language.

## The NexScript Compiler
NexScript features a compiler as a standalone command line tool, called `nexc`. It can be installed through npm and used to compile `.nex` files into `.json` artifact files. These artifact files can be imported into the NexScript JavaScript SDK (or other SDKs in the future). The `nexc` NPM package can also be imported inside JavaScript files to compile `.nex` files without using the command line tool.

### Installation
```bash
npm install -g nexc
```

### Usage
```bash
Usage: nexc [options] [source_file]

Options:
  -V, --version        Output the version number.
  -o, --output <path>  Specify a file to output the generated artifact.
  -h, --hex            Compile the contract to hex format rather than a full artifact.
  -A, --asm            Compile the contract to ASM format rather than a full artifact.
  -c, --opcount        Display the number of opcodes in the compiled bytecode.
  -s, --size           Display the size in bytes of the compiled bytecode.
  -?, --help           Display help
```
