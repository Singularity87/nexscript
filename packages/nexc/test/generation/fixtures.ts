import { Artifact } from '@nexscript/utils';
import fs from 'fs';
import { URL } from 'url';
import { version } from '../../src/index.js';

interface Fixture {
  fn: string,
  artifact: Artifact,
}

export const fixtures: Fixture[] = [
  {
    fn: 'p2pkh.nex',
    artifact: {
      contractName: 'P2PKH',
      constructorInputs: [{ name: 'pkh', type: 'bytes20', visible: false }],
      abi: [{ name: 'spend', inputs: [{ name: 'pk', type: 'pubkey' }, { name: 's', type: 'sig' }] }],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK '
        // require(hash160(pk) == pkh)
        + 'OP_OVER OP_HASH160 OP_EQUALVERIFY '
        // require(checkSig(s, pk))
        + 'OP_CHECKSIGVERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/p2pkh.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'reassignment.nex',
    artifact: {
      contractName: 'Reassignment',
      constructorInputs: [{ name: 'x', type: 'int', visible: false }, { name: 'y', type: 'string', visible: false }],
      abi: [{ name: 'hello', inputs: [{ name: 'pk', type: 'pubkey' }, { name: 's', type: 'sig' }] }],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK OP_FROMALTSTACK '
        // int myVariable = 10 - 4
        + 'OP_10 OP_4 OP_SUB '
        // int myOtherVariable = 20 + myVariable % 2
        + '14 OP_SWAP OP_2 OP_MOD OP_ADD '
        // require(myOtherVariable > x)
        + 'OP_LESSTHAN OP_VERIFY '
        // string hw = "Hello World"
        + '48656c6c6f20576f726c64 '
        // hw = hw + y
        + 'OP_DUP OP_ROT OP_CAT '
        // require(ripemd160(pk) == ripemd160(hw))
        + 'OP_2 OP_PICK OP_RIPEMD160 OP_SWAP OP_RIPEMD160 OP_EQUALVERIFY '
        // require(checkSig(s, pk))
        + 'OP_ROT OP_ROT OP_CHECKSIGVERIFY '
        + 'OP_DROP',
      source: fs.readFileSync(new URL('../valid-contract-files/reassignment.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'if_statement.nex',
    artifact: {
      contractName: 'IfStatement',
      constructorInputs: [{ name: 'x', type: 'int', visible: false }, { name: 'y', type: 'int', visible: false }],
      abi: [{ name: 'hello', inputs: [{ name: 'a', type: 'int' }, { name: 'b', type: 'int' }] }],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK OP_FROMALTSTACK '
        // int d = a + b
        + 'OP_2OVER OP_ADD '
        // d = d - a
        + 'OP_DUP OP_4 OP_PICK OP_SUB '
        // if (d == x - 2) {
        + 'OP_DUP OP_3 OP_ROLL OP_2 OP_SUB OP_NUMEQUAL OP_IF '
        // int c = d + b
        + 'OP_DUP OP_5 OP_PICK OP_ADD '
        // d = a + c
        + 'OP_4 OP_PICK OP_OVER OP_ADD OP_ROT OP_DROP OP_SWAP '
        // require(c > d)
        + 'OP_2DUP OP_LESSTHAN OP_VERIFY '
        // } else {
        + 'OP_DROP OP_ELSE '
        // require(d == a) }
        + 'OP_DUP OP_4 OP_PICK OP_NUMEQUALVERIFY OP_ENDIF '
        // d = d + a
        + 'OP_DUP OP_4 OP_ROLL OP_ADD '
        // require(d == y)
        + 'OP_3 OP_ROLL OP_NUMEQUALVERIFY '
        + 'OP_2DROP OP_DROP',
      source: fs.readFileSync(new URL('../valid-contract-files/if_statement.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'multifunction.nex',
    artifact: {
      contractName: 'MultiFunction',
      constructorInputs: [{ name: 'sender', type: 'pubkey', visible: false }, { name: 'recipient', type: 'pubkey', visible: false }, { name: 'timeout', type: 'int', visible: false }],
      abi: [
        { name: 'transfer', inputs: [{ name: 'recipientSig', type: 'sig' }] },
        { name: 'timeout', inputs: [{ name: 'senderSig', type: 'sig' }] },
      ],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK '
        // function transfer
        + 'OP_3 OP_PICK OP_0 OP_NUMEQUAL OP_IF '
        // require(checkSig(recipientSig, recipient))
        + 'OP_4 OP_ROLL OP_ROT OP_CHECKSIGVERIFY '
        + 'OP_2DROP OP_DROP OP_ELSE '
        // function timeout
        + 'OP_3 OP_ROLL OP_1 OP_NUMEQUALVERIFY '
        // require(checkSig(senderSig, sender))
        + 'OP_3 OP_ROLL OP_SWAP OP_CHECKSIGVERIFY '
        // require(tx.time >= timeout)
        + 'OP_SWAP OP_CHECKLOCKTIMEVERIFY OP_2DROP '
        + 'OP_ENDIF',
      source: fs.readFileSync(new URL('../valid-contract-files/multifunction.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'multifunction_if_statements.nex',
    artifact: {
      contractName: 'MultiFunctionIfStatements',
      constructorInputs: [{ name: 'x', type: 'int', visible: false }, { name: 'y', type: 'int', visible: false }],
      abi: [
        { name: 'transfer', inputs: [{ name: 'a', type: 'int' }, { name: 'b', type: 'int' }] },
        { name: 'timeout', inputs: [{ name: 'b', type: 'int' }] },
      ],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK OP_FROMALTSTACK '
        // function transfer
        + 'OP_2 OP_PICK OP_0 OP_NUMEQUAL OP_IF '
        // int d = a + b
        + 'OP_3 OP_PICK OP_5 OP_PICK OP_ADD '
        // d = d - a
        + 'OP_DUP OP_5 OP_PICK OP_SUB '
        // if (d == x) {
        + 'OP_DUP OP_3 OP_ROLL OP_NUMEQUAL OP_IF '
        // int c = d + b
        + 'OP_DUP OP_6 OP_PICK OP_ADD '
        // d = a + c
        + 'OP_5 OP_PICK OP_OVER OP_ADD OP_ROT OP_DROP OP_SWAP '
        // require(c > d)
        + 'OP_2DUP OP_LESSTHAN OP_VERIFY '
        // } else {
        + 'OP_DROP OP_ELSE '
        // d = a }
        + 'OP_4 OP_PICK OP_NIP OP_ENDIF '
        // d = d + a
        + 'OP_DUP OP_5 OP_ROLL OP_ADD '
        // require(d == y)
        + 'OP_3 OP_ROLL OP_NUMEQUALVERIFY '
        + 'OP_2DROP OP_2DROP OP_ELSE '
        // function timeout
        + 'OP_ROT OP_1 OP_NUMEQUALVERIFY '
        // int d = b
        + 'OP_2 OP_PICK '
        // d = d + 2
        + 'OP_DUP OP_2 OP_ADD '
        // if (d == x) {
        + 'OP_DUP OP_3 OP_ROLL OP_NUMEQUAL OP_IF '
        // int c = d + b
        + 'OP_DUP OP_4 OP_PICK OP_ADD '
        // d = c + d
        + 'OP_2DUP OP_ADD OP_ROT OP_DROP OP_SWAP '
        // require(c > d) }
        + 'OP_2DUP OP_LESSTHAN OP_VERIFY '
        + 'OP_DROP OP_ENDIF '
        // d = b
        + ''
        // require(d == y)
        + 'OP_2SWAP OP_NUMEQUALVERIFY '
        + 'OP_2DROP OP_ENDIF',
      source: fs.readFileSync(new URL('../valid-contract-files/multifunction_if_statements.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: '2_of_3_multisig.nex',
    artifact: {
      contractName: 'MultiSig',
      constructorInputs: [{ name: 'pk1', type: 'pubkey', visible: false }, { name: 'pk2', type: 'pubkey', visible: false }, { name: 'pk3', type: 'pubkey', visible: false }],
      abi: [{ name: 'spend', inputs: [{ name: 's1', type: 'sig' }, { name: 's2', type: 'sig' }] }],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK '
        // require(checkMultiSig([s1, s2], [pk1, pk2, pk3]))
        + 'OP_0 OP_2ROT OP_SWAP OP_2 OP_2ROT OP_SWAP OP_6 OP_ROLL OP_3 OP_CHECKMULTISIGVERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/2_of_3_multisig.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'split_size.nex',
    artifact: {
      contractName: 'SplitSize',
      constructorInputs: [{ name: 'b', type: 'bytes', visible: false }],
      abi: [{ name: 'spend', inputs: [] }],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK '
        // bytes x = b.split(b.length / 2)[1]
        + 'OP_DUP OP_DUP OP_SIZE OP_NIP OP_2 OP_DIV OP_SPLIT OP_NIP '
        // require(x != b)
        + 'OP_2DUP OP_EQUAL OP_NOT OP_VERIFY '
        // bytes x = b.split(b.length / 2)[1]
        + 'OP_SWAP OP_4 OP_SPLIT OP_DROP OP_EQUAL OP_NOT OP_VERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/split_size.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'cast_hash_checksig.nex',
    artifact: {
      contractName: 'CastHashChecksig',
      constructorInputs: [],
      abi: [{ name: 'hello', inputs: [{ name: 'pk', type: 'pubkey' }, { name: 's', type: 'sig' }] }],
      bytecode:
        // require((ripemd160(bytes(pk)) == hash160(0x0) == !true));
        'OP_DUP OP_RIPEMD160 OP_0 OP_HASH160 OP_EQUAL OP_1 OP_NOT OP_EQUALVERIFY '
        // require(checkSig(s, pk));
        + 'OP_CHECKSIGVERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/cast_hash_checksig.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'hodl_vault.nex',
    artifact: {
      contractName: 'HodlVault',
      constructorInputs: [
        { name: 'ownerPk', type: 'pubkey', visible: false },
        { name: 'oraclePk', type: 'pubkey', visible: false },
        { name: 'minBlock', type: 'int', visible: false },
        { name: 'priceTarget', type: 'int', visible: false },
      ],
      abi: [
        {
          name: 'spend',
          inputs: [
            { name: 'ownerSig', type: 'sig' },
            { name: 'oracleSig', type: 'datasig' },
            { name: 'oracleMessage', type: 'bytes8' },
          ],
        },
      ],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK '
        // bytes4 blockHeightBin, bytes4 priceBin = oracleMessage.split(4);
        + 'OP_6 OP_PICK OP_4 OP_SPLIT '
        // int blockHeight = int(blockHeightBin);
        + 'OP_SWAP OP_BIN2NUM '
        // int price = int(priceBin);
        + 'OP_SWAP OP_BIN2NUM '
        // require(blockHeight >= minBlock);
        + 'OP_OVER OP_5 OP_ROLL OP_GREATERTHANOREQUAL OP_VERIFY '
        // require(tx.time >= blockHeight);
        + 'OP_SWAP OP_CHECKLOCKTIMEVERIFY OP_DROP '
        // require(price >= priceTarget);
        + 'OP_3 OP_ROLL OP_GREATERTHANOREQUAL OP_VERIFY '
        // require(checkDataSig(oracleSig, oracleMessage, oraclePk));
        + 'OP_3 OP_ROLL OP_4 OP_ROLL OP_3 OP_ROLL OP_CHECKDATASIGVERIFY '
        // require(checkSig(ownerSig, ownerPk));
        + 'OP_CHECKSIGVERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/hodl_vault.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'deep_replace.nex',
    artifact: {
      contractName: 'DeepReplace',
      constructorInputs: [],
      abi: [{ name: 'hello', inputs: [] }],
      bytecode:
        // int a = 1; int b = 2; int c = 3; int d = 4; int e = 5; int f = 6;
        'OP_1 OP_2 OP_3 OP_4 OP_5 OP_6 '
        // if (a < 3) {
        + 'OP_5 OP_PICK OP_3 OP_LESSTHAN OP_IF '
        // a = 3 }
        + 'OP_3 OP_6 OP_ROLL OP_DROP OP_SWAP OP_TOALTSTACK OP_SWAP OP_TOALTSTACK OP_SWAP '
        + 'OP_TOALTSTACK OP_SWAP OP_TOALTSTACK OP_SWAP OP_FROMALTSTACK OP_FROMALTSTACK '
        + 'OP_FROMALTSTACK OP_FROMALTSTACK OP_ENDIF '
        // require(a > b + c + d + e + f);
        + 'OP_2ROT OP_5 OP_ROLL OP_ADD OP_4 OP_ROLL OP_ADD '
        + 'OP_3 OP_ROLL OP_ADD OP_ROT OP_ADD OP_GREATERTHAN OP_VERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/deep_replace.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'bounded_bytes.nex',
    artifact: {
      contractName: 'BoundedBytes',
      constructorInputs: [],
      abi: [{ name: 'spend', inputs: [{ name: 'b', type: 'bytes4' }, { name: 'i', type: 'int' }] }],
      bytecode: 'OP_SWAP OP_4 OP_NUM2BIN OP_EQUALVERIFY', // require(b == bytes4(i))
      source: fs.readFileSync(new URL('../valid-contract-files/bounded_bytes.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'covenant.nex',
    artifact: {
      contractName: 'Covenant',
      constructorInputs: [
        {
          name: 'requiredVersion',
          type: 'int',
          visible: false,
        },
      ],
      abi: [{ name: 'spend', inputs: [] }],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK '
        // require(tx.version == requiredVersion)
        + 'OP_TXVERSION OP_NUMEQUALVERIFY '
        // require(tx.bytecode == 0x00)
        + 'OP_ACTIVEBYTECODE 00 OP_EQUALVERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/covenant.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'covenant_all_fields.nex',
    artifact: {
      contractName: 'Covenant',
      constructorInputs: [],
      abi: [{ name: 'spend', inputs: [] }],
      bytecode:
        // require(tx.version == 2)
        'OP_TXVERSION OP_2 OP_NUMEQUALVERIFY '
        // require(tx.locktime == 0)
        + 'OP_TXLOCKTIME OP_0 OP_NUMEQUALVERIFY '
        // require(tx.inputs.length == 1)
        + 'OP_TXINPUTCOUNT OP_1 OP_NUMEQUALVERIFY '
        // require(tx.outputs.length == 1)
        + 'OP_TXOUTPUTCOUNT OP_1 OP_NUMEQUALVERIFY '
        // require(this.activeInputIndex == 0)
        + 'OP_INPUTINDEX OP_0 OP_NUMEQUALVERIFY '
        // require(this.activeBytecode.length == 300)
        + 'OP_ACTIVEBYTECODE OP_SIZE OP_NIP 2c01 OP_NUMEQUALVERIFY '
        // require(tx.inputs[0].value == 10000)
        + 'OP_0 OP_UTXOVALUE 1027 OP_NUMEQUALVERIFY '
        // require(tx.inputs[0].lockingBytecode.length == 10000)
        + 'OP_0 OP_UTXOBYTECODE OP_SIZE OP_NIP 1027 OP_NUMEQUALVERIFY '
        // require(tx.inputs[0].outpointTransactionHash == 0x00...00)
        + 'OP_0 OP_OUTPOINTTXHASH 0000000000000000000000000000000000000000000000000000000000000000 OP_EQUALVERIFY '
        // require(tx.inputs[0].outpointIndex == 0)
        + 'OP_0 OP_OUTPOINTINDEX OP_0 OP_NUMEQUALVERIFY '
        // require(tx.inputs[0].unlockingBytecode.length == 100)
        + 'OP_0 OP_INPUTBYTECODE OP_SIZE OP_NIP 64 OP_NUMEQUALVERIFY '
        // require(tx.inputs[0].sequenceNumber == 0)
        + 'OP_0 OP_INPUTSEQUENCENUMBER OP_0 OP_NUMEQUALVERIFY '
        // require(tx.outputs[0].value == 10000)
        + 'OP_0 OP_OUTPUTVALUE 1027 OP_NUMEQUALVERIFY '
        // require(tx.outputs[0].lockingBytecode.length == 100)
        + 'OP_0 OP_OUTPUTBYTECODE OP_SIZE OP_NIP 64 OP_NUMEQUALVERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/covenant_all_fields.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'mecenas.nex',
    artifact: {
      contractName: 'Mecenas',
      constructorInputs: [
        { name: 'recipient', type: 'bytes20', visible: false },
        { name: 'funder', type: 'bytes20', visible: false },
        { name: 'pledge', type: 'int', visible: false },
        { name: 'period', type: 'int', visible: false },
      ],
      abi: [
        { name: 'receive', inputs: [] },
        { name: 'reclaim', inputs: [{ name: 'pk', type: 'pubkey' }, { name: 's', type: 'sig' }] },
      ],
      bytecode:
        // pull constructor parameters from altstack
        'OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK '
        // function receive
        + 'OP_4 OP_PICK OP_0 OP_NUMEQUAL OP_IF '
        // require(tx.age >= period)
        + 'OP_3 OP_ROLL OP_CHECKSEQUENCEVERIFY OP_DROP '
        // require(tx.outputs[0].lockingBytecode == new LockingBytecodeP2PKT(recipient))
        + 'OP_0 OP_OUTPUTBYTECODE 005114 OP_ROT OP_CAT OP_EQUALVERIFY '
        // int minerFee = 1000
        + 'e803 '
        // int currentValue = tx.inputs[this.activeInputIndex].value
        + 'OP_INPUTINDEX OP_UTXOVALUE '
        // int changeValue = currentValue - pledge - minerFee
        + 'OP_DUP OP_4 OP_PICK OP_SUB OP_2 OP_PICK OP_SUB '
        // if (changeValue <= pledge + minerFee) {
        + 'OP_DUP OP_5 OP_PICK OP_4 OP_PICK OP_ADD OP_LESSTHANOREQUAL OP_IF '
        // require(tx.outputs[0].value == currentValue - minerFee)
        + 'OP_0 OP_OUTPUTVALUE OP_2OVER OP_SWAP OP_SUB OP_NUMEQUALVERIFY '
        // } else {
        + 'OP_ELSE '
        // require(tx.outputs[0].value == pledge)
        + 'OP_0 OP_OUTPUTVALUE OP_5 OP_PICK OP_NUMEQUALVERIFY '
        // require(
        //   tx.outputs[1].lockingBytecode == tx.inputs[this.activeInputIndex].lockingBytecode
        // )
        + 'OP_1 OP_OUTPUTBYTECODE OP_INPUTINDEX OP_UTXOBYTECODE OP_EQUALVERIFY '
        // require(tx.outputs[1].value == changeValue) }
        + 'OP_1 OP_OUTPUTVALUE OP_OVER OP_NUMEQUALVERIFY '
        // Cleanup
        + 'OP_ENDIF OP_2DROP OP_2DROP OP_2DROP OP_ELSE '
        // function reclaim
        + 'OP_4 OP_ROLL OP_1 OP_NUMEQUALVERIFY '
        // require(hash160(pk) == funder)
        + 'OP_4 OP_PICK OP_HASH160 OP_ROT OP_EQUALVERIFY '
        // require(checkSig(s, pk))
        + 'OP_4 OP_ROLL OP_4 OP_ROLL OP_CHECKSIGVERIFY '
        // Cleanup
        + 'OP_2DROP OP_DROP OP_ENDIF',
      source: fs.readFileSync(new URL('../valid-contract-files/mecenas.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'announcement.nex',
    artifact: {
      contractName: 'Announcement',
      constructorInputs: [],
      abi: [{ name: 'announce', inputs: [] }],
      bytecode:
        // bytes announcement = new LockingBytecodeNullData(...)
        '6a 6d02 OP_SIZE OP_SWAP OP_CAT OP_CAT '
        + '4120636f6e7472616374206d6179206e6f7420696e6a75726520612068756d616e20626'
        + '5696e67206f722c207468726f75676820696e616374696f6e2c20616c6c6f77206120687'
        + '56d616e206265696e6720746f20636f6d6520746f206861726d2e '
        + 'OP_SIZE OP_DUP 4b OP_GREATERTHAN OP_IF 4c OP_SWAP OP_CAT OP_ENDIF OP_SWAP OP_CAT OP_CAT '
        // require(tx.outputs[0].value == 0)
        + 'OP_0 OP_OUTPUTVALUE OP_0 OP_NUMEQUALVERIFY '
        // require(tx.outputs[0].lockingBytecode == announcement)
        + 'OP_0 OP_OUTPUTBYTECODE OP_EQUALVERIFY '
        // int minerFee = 1000
        + 'e803 '
        // int changeAmount = tx.inputs[this.activeInputIndex].value - minerFee
        + 'OP_INPUTINDEX OP_UTXOVALUE OP_OVER OP_SUB '
        // if (changeAmount >= minerFee)
        + 'OP_DUP OP_ROT OP_GREATERTHANOREQUAL OP_IF '
        // require(
        //  tx.outputs[1].lockingBytecode == tx.inputs[this.activeInputIndex].lockingBytecode
        // )
        + 'OP_1 OP_OUTPUTBYTECODE OP_INPUTINDEX OP_UTXOBYTECODE OP_EQUALVERIFY '
        // require(tx.outputs[1].value == changeAmount) }
        + 'OP_1 OP_OUTPUTVALUE OP_OVER OP_NUMEQUALVERIFY OP_ENDIF '
        // Stack clean-up
        + 'OP_DROP',
      source: fs.readFileSync(new URL('../valid-contract-files/announcement.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'p2palindrome.nex',
    artifact: {
      contractName: 'P2Palindrome',
      constructorInputs: [],
      abi: [
        { name: 'spend', inputs: [{ name: 'palindrome', type: 'string' }] },
      ],
      bytecode: 'OP_DUP OP_REVERSEBYTES OP_EQUALVERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/p2palindrome.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'num2bin_variable.nex',
    artifact: {
      contractName: 'Num2Bin',
      constructorInputs: [],
      abi: [
        { name: 'spend', inputs: [{ name: 'size', type: 'int' }] },
      ],
      bytecode: 'OP_10 OP_SWAP OP_NUM2BIN OP_BIN2NUM OP_10 OP_NUMEQUALVERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/num2bin_variable.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },
  {
    fn: 'number_units.nex',
    artifact: {
      contractName: 'NumberUnits',
      constructorInputs: [],
      abi: [{ name: 'test', inputs: [] }],
      bytecode: 'OP_1 OP_1 OP_NUMEQUALVERIFY 64 64 OP_NUMEQUALVERIFY 00e1f505 00e1f505 OP_NUMEQUALVERIFY 00e1f505 00e1f505 OP_NUMEQUALVERIFY OP_1 OP_1 OP_NUMEQUALVERIFY 3c 3c OP_NUMEQUALVERIFY 100e 100e OP_NUMEQUALVERIFY 805101 805101 OP_NUMEQUALVERIFY 803a09 803a09 OP_NUMEQUALVERIFY',
      source: fs.readFileSync(new URL('../valid-contract-files/number_units.nex', import.meta.url), { encoding: 'utf-8' }),
      compiler: {
        name: 'nexc',
        version,
      },
      updatedAt: '',
    },
  },

  // TODO: Add fixtures with visible constructor parameters
];
