import { binToHex } from '@bitauth/libauth';
import {
  AbiFunction,
  Artifact,
  asmToScript,
  calculateBytesize,
  countOpcodes,
  generateRedeemScript,
  minimallyEncodeData,
  PrimitiveType,
  Script,
  scriptToBytecode,
} from '@nexscript/utils';
import { Transaction } from './Transaction.js';
import { Argument, encodeArgument } from './Argument.js';
import { Utxo } from './interfaces.js';
import NetworkProvider from './network/NetworkProvider.js';
import {
  scriptTemplateToAddress,
} from './utils.js';
import SignatureTemplate from './SignatureTemplate.js';
import { ElectrumNetworkProvider } from './network/index.js';

export class Contract {
  name: string;
  address: string;
  bytecode: string;
  bytesize: number;
  opcount: number;

  functions: {
    [name: string]: ContractFunction,
  };

  private redeemScript: Script;
  private templateScript: Script;
  private constraintScript: Script;
  private visibleArgs: Uint8Array[] = [];

  constructor(
    private artifact: Artifact,
    constructorArgs: Argument[],
    private provider: NetworkProvider = new ElectrumNetworkProvider(),
  ) {
    const expectedProperties = ['abi', 'bytecode', 'constructorInputs', 'contractName'];
    if (!expectedProperties.every((property) => property in artifact)) {
      throw new Error('Invalid or incomplete artifact provided');
    }

    if (artifact.constructorInputs.length !== constructorArgs.length) {
      throw new Error(`Incorrect number of arguments passed to ${artifact.contractName} constructor`);
    }

    // Encode arguments (this also performs type checking)
    const encodedArgs = constructorArgs
      .map((arg, i) => encodeArgument(arg, artifact.constructorInputs[i].type) as Uint8Array)
      .filter((arg, i) => !artifact.constructorInputs[i].visible);

    this.visibleArgs = constructorArgs
      .map((arg, i) => encodeArgument(arg, artifact.constructorInputs[i].type) as Uint8Array)
      .filter((arg, i) => artifact.constructorInputs[i].visible);

    this.visibleArgs = minimallyEncodeData(this.visibleArgs);

    // Check there's no signature templates in the constructor
    if (encodedArgs.some((arg) => arg instanceof SignatureTemplate)) {
      throw new Error('Cannot use signatures in constructor');
    }

    this.templateScript = asmToScript(this.artifact.bytecode);
    this.constraintScript = minimallyEncodeData(encodedArgs);

    this.redeemScript = generateRedeemScript(
      this.templateScript,
      encodedArgs as Uint8Array[],
    );

    // Populate the functions object with the contract's functions
    // (with a special case for single function, which has no "function selector")
    this.functions = {};
    if (artifact.abi.length === 1) {
      const f = artifact.abi[0];
      this.functions[f.name] = this.createFunction(f);
    } else {
      artifact.abi.forEach((f, i) => {
        this.functions[f.name] = this.createFunction(f, i);
      });
    }

    this.name = artifact.contractName;
    this.address = scriptTemplateToAddress(
      this.templateScript,
      this.constraintScript,
      this.visibleArgs,
      this.provider.network
    );
    this.bytecode = binToHex(scriptToBytecode(this.templateScript));
    this.bytesize = calculateBytesize(this.templateScript);
    this.opcount = countOpcodes(this.templateScript);
  }

  async getBalance(): Promise<bigint> {
    const utxos = await this.getUtxos();
    return utxos.reduce((acc, utxo) => acc + utxo.satoshis, 0n);
  }

  async getUtxos(): Promise<Utxo[]> {
    return this.provider.getUtxos(this.address);
  }

  private createFunction(abiFunction: AbiFunction, selector?: number): ContractFunction {
    return (...args: Argument[]) => {
      if (abiFunction.inputs.length !== args.length) {
        throw new Error(`Incorrect number of arguments passed to function ${abiFunction.name}`);
      }

      // Encode passed args (this also performs type checking)
      let encodedArgs = args
        .map((arg, i) => encodeArgument(arg, abiFunction.inputs[i].type));

      if (selector !== undefined) {
        encodedArgs = [encodeArgument(BigInt(selector), PrimitiveType.INT), ...encodedArgs]
      }

      return new Transaction(
        this.address,
        this.provider,
        this.redeemScript,
        abiFunction,
        encodedArgs,
        this.templateScript,
        this.constraintScript,
        this.visibleArgs,
      );
    };
  }
}

export type ContractFunction = (...args: Argument[]) => Transaction;
