import {
  binToHex,
} from '@bitauth/libauth';
import delay from 'delay';
import {
  AbiFunction,
  Op,
  placeholder,
  Script,
  scriptToBytecode,
} from '@nexscript/utils';
import {
  Utxo,
  Output,
  Recipient,
  isSignableUtxo,
  TransactionDetails,
  SignableUtxo,
} from './interfaces.js';
import {
  meep,
  getInputSize,
  createOpReturnOutput,
  getTxSizeWithoutInputs,
  buildError,
  validateRecipient,
  utxoComparator,
} from './utils.js';
import { DUST_LIMIT, p2stOutputSize } from './constants.js';
import NetworkProvider from './network/NetworkProvider.js';
import SignatureTemplate from './SignatureTemplate.js';
import nexcore from 'nexcore-lib';

const bip68 = await import('bip68');

export class Transaction {
  private inputs: Utxo[] = [];
  private outputs: Output[] = [];

  private txid = "";

  private sequence = 0xfffffffe;
  private locktime: number;
  private feePerByte: number = 2.0;
  private hardcodedFee: bigint;
  private minChange: bigint = DUST_LIMIT;

  constructor(
    private address: string,
    private provider: NetworkProvider,
    private redeemScript: Script,
    private abiFunction: AbiFunction,
    private args: (Uint8Array | SignatureTemplate)[],
    private templateScript: Script,
    private constraintScript: Script,
    private visibleArgs: Uint8Array[],
  ) {}

  from(input: Utxo): this;
  from(inputs: Utxo[]): this;

  from(inputOrInputs: Utxo | Utxo[]): this {
    if (!Array.isArray(inputOrInputs)) {
      inputOrInputs = [inputOrInputs];
    }

    this.inputs = this.inputs.concat(inputOrInputs);

    return this;
  }

  fromP2PKT(input: Utxo, template: SignatureTemplate): this;
  fromP2PKT(inputs: Utxo[], template: SignatureTemplate): this;

  fromP2PKT(inputOrInputs: Utxo | Utxo[], template: SignatureTemplate): this {
    if (!Array.isArray(inputOrInputs)) {
      inputOrInputs = [inputOrInputs];
    }

    inputOrInputs = inputOrInputs.map((input) => ({ ...input, template }));

    this.inputs = this.inputs.concat(inputOrInputs);

    return this;
  }

  to(to: string, amount: bigint): this;
  to(outputs: Recipient[]): this;

  to(toOrOutputs: string | Recipient[], amount?: bigint): this {
    if (typeof toOrOutputs === 'string' && typeof amount === 'bigint') {
      return this.to([{ to: toOrOutputs, amount }]);
    }

    if (Array.isArray(toOrOutputs) && amount === undefined) {
      toOrOutputs.forEach(validateRecipient);
      this.outputs = this.outputs.concat(toOrOutputs);
      return this;
    }

    throw new Error('Incorrect arguments passed to function \'to\'');
  }

  withOpReturn(chunks: string[]): this {
    this.outputs.push(createOpReturnOutput(chunks));
    return this;
  }

  withAge(age: number): this {
    this.sequence = bip68.encode({ blocks: age });
    return this;
  }

  withTime(time: number): this {
    this.locktime = time;
    return this;
  }

  withHardcodedFee(hardcodedFee: bigint): this {
    this.hardcodedFee = hardcodedFee;
    return this;
  }

  withFeePerByte(feePerByte: number): this {
    this.feePerByte = feePerByte;
    return this;
  }

  withMinChange(minChange: bigint): this {
    this.minChange = minChange;
    return this;
  }

  withoutChange(): this {
    return this.withMinChange(BigInt(Number.MAX_VALUE));
  }

  async build(): Promise<string> {
    this.locktime = this.locktime ?? await this.provider.getBlockHeight();
    await this.setInputsAndOutputs();

    const transaction = new nexcore.Transaction();

    this.outputs.forEach((output) => {
      if (output.to instanceof Uint8Array) {
        transaction.addOutput(new nexcore.Transaction.Output({
          type: 0,
          script: nexcore.Script(binToHex(output.to)),
          satoshis: 0
        }));
      } else {
        transaction.to(output.to, Number(output.amount), 1); // p2st
      }
    });

    // first add all inputs to complete transaction layout, then sign them in next loop
    this.inputs.forEach((utxo) => {
      // UTXO's with signature templates are signed using P2PKT
      const txo = {
        txId: utxo.txid,
        outputIndex: utxo.vout,
        satoshis: Number(utxo.satoshis),
        address: utxo.address
      } as any;

      if (isSignableUtxo(utxo)) {
        transaction.from(txo);
        return;
      }

      // contract input
      const templateScript = nexcore.Script.fromHex(binToHex(scriptToBytecode(this.templateScript)));
      const constraintScript = this.constraintScript.length ? nexcore.Script.fromHex(binToHex(scriptToBytecode(this.constraintScript))) : nexcore.Opcode.OP_0;
      const visibleArgs = this.visibleArgs;

      const unspentOutput = new nexcore.Transaction.UnspentOutput(txo);
      const input = new nexcore.Transaction.Input.ScriptTemplate({
        type: nexcore.Transaction.Input.DEFAULT_TYPE,
        output: new nexcore.Transaction.Output({
          script: unspentOutput.script,
          satoshis: unspentOutput.satoshis
        }),
        prevTxId: unspentOutput.txId,
        outputIndex: unspentOutput.outputIndex,
        script: nexcore.Script.empty(),
        amount: unspentOutput.satoshis,
        templateScript: templateScript,
        constraintScript: constraintScript,
        visibleArgs: visibleArgs
      }, templateScript, constraintScript, visibleArgs, undefined);
      transaction.addInput(input);
    });

    // sequences and locktime must be set before producing signatures
    if (this.locktime < 5e8) {
      transaction.lockUntilBlockHeight(this.locktime);
    } else {
      transaction.lockUntilDate(this.locktime);
    }

    this.inputs.forEach((utxo, index) => {
      if (isSignableUtxo(utxo)) {
        const privateKey = (utxo as SignableUtxo).template.privateKey;
        const nexPrivateKey = nexcore.PrivateKey(binToHex(privateKey), this.provider.network);

        const signatures = transaction.inputs[index].getSignatures(transaction, nexPrivateKey, index, nexcore.crypto.Signature.SIGHASH_NEXA_ALL);
        transaction.inputs[index].addSignature(transaction, signatures[0], "schnorr");
        return;
      }

      const completeArgs = this.args.slice().reverse().map((arg) => {
        if (!(arg instanceof SignatureTemplate)) return arg;

        const privateKey = (arg as SignatureTemplate).privateKey;
        const nexPrivateKey = nexcore.PrivateKey(binToHex(privateKey), this.provider.network);

        const signature = nexcore.Transaction.sighash.sign(transaction, nexPrivateKey, nexcore.crypto.Signature.SIGHASH_NEXA_ALL, index, transaction.inputs[index].templateScript);

        return Uint8Array.from([...signature.toDER()]);
      });

      const satisfierScript =  completeArgs.length ? nexcore.Script.fromHex(binToHex(scriptToBytecode(completeArgs))) : nexcore.Script.empty();

      transaction.inputs[index].setScript(nexcore.Script.buildScriptTemplateIn(
        transaction.inputs[index].templateScript,
        transaction.inputs[index].constraintScript?.chunks?.length ? transaction.inputs[index].constraintScript : Op.OP_0,
        satisfierScript.toBuffer()
      ));
    });

    const serialized = transaction.serialize();
    // rostrum issue. save txid for later query
    this.txid = transaction.id;
    return serialized;
  }

  async send(): Promise<TransactionDetails>;
  async send(raw: true): Promise<string>;

  async send(raw?: true): Promise<TransactionDetails | string> {
    const tx = await this.build();
    try {
      // rostrum issue. use txid and not the returned idem to get the transaction
      const txid = this.txid;
      await this.provider.sendRawTransaction(tx);
      return raw ? await this.getTxDetails(txid, raw) : await this.getTxDetails(txid);
    } catch (e: any) {
      const reason = e.error ?? e.message;
      throw buildError(reason, meep(tx, this.inputs, this.redeemScript));
    }
  }

  private async getTxDetails(txid: string): Promise<TransactionDetails>
  private async getTxDetails(txid: string, raw: true): Promise<string>;

  private async getTxDetails(txid: string, raw?: true): Promise<TransactionDetails | string> {
    for (let retries = 0; retries < 1200; retries += 1) {
      await delay(500);
      try {
        const hex = await this.provider.getRawTransaction(txid);

        if (raw) return hex;

        const nexcoreTransaction = nexcore.Transaction(hex);
        return { ...nexcoreTransaction, txid, hex };
      } catch (ignored) {
        // ignored
      }
    }

    // Should not happen
    throw new Error('Could not retrieve transaction details for over 10 minutes');
  }

  async meep(): Promise<string> {
    const tx = await this.build();
    return meep(tx, this.inputs, this.redeemScript);
  }

  private async setInputsAndOutputs(): Promise<void> {
    if (this.outputs.length === 0) {
      throw Error('Attempted to build a transaction without outputs');
    }

    // Replace all SignatureTemplate with 65-length placeholder Uint8Arrays
    const placeholderArgs = this.args.map((arg) => (
      arg instanceof SignatureTemplate ? placeholder(65) : arg
    ));

    const templateScript = nexcore.Script.fromHex(binToHex(scriptToBytecode(this.templateScript)));
    const constraintScript = this.constraintScript.length ? nexcore.Script.fromHex(binToHex(scriptToBytecode(this.constraintScript))) : nexcore.Opcode.OP_0;
    const satisfierScript = placeholderArgs.length ? nexcore.Script.fromHex(binToHex(scriptToBytecode(placeholderArgs))) : nexcore.Script.empty();


    const scriptTemplateIn = nexcore.Script.buildScriptTemplateIn(
      templateScript,
      constraintScript.chunks?.length ? constraintScript : Op.OP_0,
      satisfierScript.toBuffer()
    );

    // Add one extra byte per input to over-estimate tx-in count
    const inputSize = getInputSize(scriptTemplateIn.toBuffer()) + 1;

    // Note that we use the addPrecision function to add "decimal points" to BigInt numbers

    // Calculate amount to send and base fee (excluding additional fees per UTXO)
    let amount = addPrecision(this.outputs.reduce((acc, output) => acc + output.amount, 0n));
    let fee = addPrecision(this.hardcodedFee ?? getTxSizeWithoutInputs(this.outputs) * this.feePerByte);

    // Select and gather UTXOs and calculate fees and available funds
    let satsAvailable = 0n;
    if (this.inputs.length > 0) {
      // If inputs are already defined, the user provided the UTXOs and we perform no further UTXO selection
      if (!this.hardcodedFee) fee += addPrecision(this.inputs.length * inputSize * this.feePerByte);
      satsAvailable = addPrecision(this.inputs.reduce((acc, input) => acc + input.satoshis, 0n));
    } else {
      // If inputs are not defined yet, we retrieve the contract's UTXOs and perform selection
      const utxos = await this.provider.getUtxos(this.address);

      // We sort the UTXOs mainly so there is consistent behaviour between network providers
      // even if they report UTXOs in a different order
      utxos.sort(utxoComparator).reverse();

      for (const utxo of utxos) {
        this.inputs.push(utxo);
        satsAvailable += addPrecision(utxo.satoshis);
        if (!this.hardcodedFee) fee += addPrecision(inputSize * this.feePerByte);
        if (satsAvailable > amount + fee) break;
      }
    }

    // Remove "decimal points" from BigInt numbers (rounding up for fee, down for others)
    satsAvailable = removePrecisionFloor(satsAvailable);
    amount = removePrecisionFloor(amount);
    fee = removePrecisionCeil(fee);

    // Calculate change and check available funds
    let change = satsAvailable - amount - fee;

    if (change < 0) {
      throw new Error(`Insufficient funds: available (${satsAvailable}) < needed (${amount + fee}).`);
    }

    // Account for the fee of adding a change output
    if (!this.hardcodedFee) {
      const outputSize = p2stOutputSize(this.address)
      change -= BigInt(outputSize * this.feePerByte);
    }

    // Add a change output if applicable
    if (change >= DUST_LIMIT && change >= this.minChange) {
      this.outputs.push({ to: this.address, amount: change });
    }
  }
}

// Note: the below is a very simple implementation of a "decimal point" system for BigInt numbers
// It is safe to use for UTXO fee calculations due to its low numbers, but should not be used for other purposes
// Also note that multiplication and division between two "decimal" bigints is not supported

// High precision may not work with some 'number' inputs, so we set the default to 6 "decimal places"
const addPrecision = (amount: number | bigint, precision: number = 6): bigint => {
  if (typeof amount === 'number') {
    return BigInt(Math.ceil(amount * 10 ** precision));
  }

  return amount * BigInt(10 ** precision);
};

const removePrecisionFloor = (amount: bigint, precision: number = 6): bigint => (
  amount / (10n ** BigInt(precision))
);

const removePrecisionCeil = (amount: bigint, precision: number = 6): bigint => {
  const multiplier = 10n ** BigInt(precision);
  return (amount + multiplier - 1n) / multiplier;
};
