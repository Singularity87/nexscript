import {
  binToHex,
  deriveHdPath,
  deriveHdPrivateNodeFromSeed,
  hash160,
  hexToBin,
  secp256k1,
} from '@bitauth/libauth';
import bip39 from 'bip39';
import { PriceOracle } from './PriceOracle.js';
import { Network } from '../../src/interfaces.js';
import nexcore from 'nexcore-lib';
import { ElectrumNetworkProvider } from '../../src/index.js';

export const network = globalThis.process?.env?.JEST_WORKER_ID === undefined ? Network.MAINNET : Network.REGTEST;

const seed = await bip39.mnemonicToSeed('NexScript Tests');

const rootNode = deriveHdPrivateNodeFromSeed(seed, true);
const baseDerivationPath = "m/44'/145'/0'/0";

const aliceNode = deriveHdPath(rootNode, `${baseDerivationPath}/0`);
const bobNode = deriveHdPath(rootNode, `${baseDerivationPath}/1`);
if (typeof aliceNode === 'string') throw new Error();
if (typeof bobNode === 'string') throw new Error();

export const alicePriv = aliceNode.privateKey;
const alicePubNexa = nexcore.PrivateKey(binToHex(alicePriv)).publicKey;
export const alicePub = new Uint8Array(alicePubNexa.toBuffer());
const aliceAddressObj = alicePubNexa.toAddress(nexcore.Networks.regtest);
export const aliceAddress = aliceAddressObj.toString();
export const alicePkh = new Uint8Array(aliceAddressObj.hashBuffer).slice(4);

export const bobPriv = bobNode.privateKey;
const bobPubNexa = nexcore.PrivateKey(binToHex(bobPriv)).publicKey;
export const bobPub = new Uint8Array(bobPubNexa.toBuffer());
const bobAddressObj = bobPubNexa.toAddress(nexcore.Networks.regtest);
export const bobAddress = bobAddressObj.toString();
export const bobPkh = new Uint8Array(bobAddressObj.hashBuffer).slice(4);

export const oracle = new PriceOracle(bobPriv);
export const oraclePub = bobPub;

console.log(aliceAddress, bobAddress);

export const fund = async (address: string, satoshis: number) => {
  const provider = new ElectrumNetworkProvider();
  const utxos = await provider.getUtxos(aliceAddress);

  const tx = new nexcore.Transaction();
  const utxo = utxos.filter(val => val.satoshis > satoshis)[0];
  const txo = {
    txId: utxo.txid,
    outputIndex: utxo.vout,
    satoshis: Number(utxo.satoshis),
    address: utxo.address
  }

  tx.from(txo);
  tx.to(address, satoshis, 1);
  tx.change(aliceAddress);

  const privateKey = nexcore.PrivateKey(binToHex(alicePriv));
  tx.sign(privateKey, nexcore.crypto.Signature.SIGHASH_NEXA_ALL, "schnorr");
  // console.log(tx.serialize());
  return provider.sendRawTransaction(tx.serialize());
}
