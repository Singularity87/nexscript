import {
  hexToBin,
} from '@bitauth/libauth';
import { Output, Network } from '../src/interfaces.js';
import { network as defaultNetwork } from './fixture/vars.js';

export function getTxOutputs(tx: any, network: Network = defaultNetwork): Output[] {
  return tx.outputs.map((o: any) => {
    const OP_RETURN = '6a';
    const scriptHex = o._scriptBuffer.toString("hex");

    if (scriptHex.startsWith(OP_RETURN)) {
      return { to: hexToBin(scriptHex), amount: 0n };
    }

    const address = o.script.toAddress(network).toNexaAddress();
    return {
      to: address,
      amount: BigInt(o.satoshis),
    };
  });
}
